﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;


        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>Фабричный метод создания объекта Partner</summary>
        public Partner CreatePartner(Guid id) =>
           new Partner()
           {
                Id = id,
                Name = "Microsoft",
                IsActive = true,
                NumberIssuedPromoCodes = 10,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.NewGuid(),
                        PartnerId = id,
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2022, 10, 9),
                        CancelDate = null,
                        Limit = 100
                    }
                }
           };

        /// <summary>Фабричный метод создания объекта SetPartnerPromoCodeLimitRequest</summary>
        public SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest() =>
            new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 50,
                EndDate = new DateTime(2022, 10, 1)
            };

        /// <summary>1. Если партнер не найден, то нужно выдать ошибку 404</summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var request = this.CreateSetPartnerPromoCodeLimitRequest();
            Partner partner = null;
            
            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// 2. Если партнер заблокирован,
        /// то есть поле IsActive=false в классе Partner, 
        /// то также нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var request = this.CreateSetPartnerPromoCodeLimitRequest();
            var partner = this.CreatePartner(partnerId).Set(p => p.IsActive = false);

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            (result as BadRequestObjectResult).Value.Should().Be("Данный партнер не активен");
        }

        /// <summary>
        /// 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesReset_True()
        {
            // Arrange
            var request = this.CreateSetPartnerPromoCodeLimitRequest();
            var partnerId = Guid.NewGuid();
            var partner = this.CreatePartner(partnerId);

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            // количество выданных промокодов обнулено
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>3. Если лимит закончился, то количество не обнуляется</summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesReset_False()
        {
            // Arrange
            var request = this.CreateSetPartnerPromoCodeLimitRequest();
            var partnerId = Guid.NewGuid();
            var partner = this.CreatePartner(partnerId)
                .Set(p => (p.PartnerLimits as List<PartnerPromoCodeLimit>).ForEach(c => c.CancelDate = DateTime.Now));
            
            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            // количество выданных промокодов не обнулено
            partner.NumberIssuedPromoCodes.Should().BeGreaterThan(0);
        }

        /// <summary>4. При установке лимита нужно отключить предыдущий лимит</summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ActiveLimits_OnlyOne()
        {
            // Arrange
            var request = this.CreateSetPartnerPromoCodeLimitRequest();
            var partnerId = Guid.NewGuid();
            var partner = this.CreatePartner(partnerId);

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            // только один активный лимит
            partner.PartnerLimits.Count(x => !x.CancelDate.HasValue).Should().Be(1);
        }

        /// <summary>5. Лимит должен быть больше 0</summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsNoGreaterThan0_ReturnsBadRequest()
        {
            // Arrange
            var request = this.CreateSetPartnerPromoCodeLimitRequest().Set(r => r.Limit = 0);
            var partnerId = Guid.NewGuid();
            var partner = this.CreatePartner(partnerId);

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            (result as BadRequestObjectResult).Value.Should().Be("Лимит должен быть больше 0");
        }

        /// <summary>6. Нужно убедиться, что сохранили новый лимит в базу данных</summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitSaved_Success()
        {
            // Arrange
            var request = this.CreateSetPartnerPromoCodeLimitRequest();
            var partnerId = Guid.NewGuid();
            var partner = this.CreatePartner(partnerId);
            
            var previousLimitsCount = partner.PartnerLimits.Count();
            var previousLimitId = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue).Id;
            
            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            _partnersRepositoryMock
                .Setup(repo => repo.UpdateAsync(partner))
                .Callback<Partner>(p =>
                {
                    var r = p.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);
                    r.Id = Guid.NewGuid();

                });

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            // общее количество лимитов увеличилось
            partner.PartnerLimits.Count().Should().BeGreaterThan(previousLimitsCount);
            // есть активный лимит (ровно один)
            partner.PartnerLimits.Count(x => !x.CancelDate.HasValue).Should().Be(1);
            // id активного лимита поменялось
            var values = (result as CreatedAtActionResult).RouteValues;
            partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue).Should().NotBe(previousLimitId);
            partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue).Should().NotBe(values["limitId"]);
        }
    }
}