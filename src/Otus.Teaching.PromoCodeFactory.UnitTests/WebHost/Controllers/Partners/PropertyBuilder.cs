﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public static class PropertyBuilder
    {
        public static T Set<T>(this T self, Action<T> setProperties) where T : class
        {
            setProperties(self);
            return self;
        }
    }
}
